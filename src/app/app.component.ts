import { Component, computed, inject } from '@angular/core';
import { RouterLink, RouterOutlet } from '@angular/router';
import { Store } from '@ngrx/store';
import { selectMsg } from './core/store/errors/errors.feature';
import { isEmpty, selectList as selectCartList, selectTotalCart, totalItems } from './core/store/cart/cart.feature';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, RouterLink],
  template: `
    @if(errorMsg()) {
      <div style="background-color: red">
        {{ errorMsg() }}
      </div>  
    }
    <button routerLink="home">home</button>
    <button routerLink="shop">shop</button>
    <button routerLink="cart">cart</button>
    <button routerLink="demo-counter">demo-counter</button>

    @if (!isEmpty()) {
      <span>( {{ totalItems() }} € {{ totalCart() }}) </span>
    }
    <hr>
    <router-outlet />
  `,
  styles: [],
})
export class AppComponent {
  store = inject(Store)
  errorMsg = this.store.selectSignal(selectMsg)

  cartItems = this.store.selectSignal(selectCartList)
  isEmpty = this.store.selectSignal(isEmpty)
  totalItems = this.store.selectSignal(totalItems)
  totalCart = this.store.selectSignal(selectTotalCart)

  // productsItems = this.store.selectSignal(selectProductList)
}
