import { provideHttpClient, withInterceptors } from '@angular/common/http';
import { ApplicationConfig, isDevMode } from '@angular/core';
import { provideRouter } from '@angular/router';

import { routes } from './app.routes';
import { provideState, provideStore } from '@ngrx/store';
import { provideStoreDevtools } from '@ngrx/store-devtools';
import { authInterceptor } from './core/auth.interceptor';
import { cartFeature } from './core/store/cart/cart.feature';
import { counterFeature } from './core/store/counter/counter.feature';
import { provideEffects } from '@ngrx/effects';
import { errorsFeature } from './core/store/errors/errors.feature';
import * as routerEffects from './core/store/router/router.effects';
import * as productsEffects from './features/shop/store/products/products.effects';
import { provideRouterStore, routerReducer } from '@ngrx/router-store';


export const appConfig: ApplicationConfig = {
  providers: [
    provideRouter(routes),
    provideHttpClient(withInterceptors([authInterceptor])),
    provideStore(),
    provideRouterStore(),
    provideState({ name: 'router', reducer: routerReducer}),
    provideState({ name: cartFeature.name, reducer: cartFeature.reducer }),
    provideState({ name: errorsFeature.name, reducer: errorsFeature.reducer }),
    provideState({ name: counterFeature.name, reducer: counterFeature.reducer }),
    //   provideState({ name: productsFeature.name, reducer: productsFeature.reducer }),
    provideState({ name: 'todos', reducer: () => [1, 2, 3] }),
    provideStoreDevtools({ maxAge: 25, logOnly: !isDevMode() }),
    provideEffects([productsEffects, routerEffects]),
    provideRouterStore()
]
};
