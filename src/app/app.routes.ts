import { Routes } from '@angular/router';
import { provideState } from '@ngrx/store';
import { shopReducer } from './features/shop/store';
import { filtersFeature } from './features/shop/store/filters/filters.feature';
import { productsFeature } from './features/shop/store/products/products.feature';

export const routes: Routes = [
  { path: 'home', loadComponent: () => import('./features/home/home.component')},
  {
    path: 'shop',
    loadComponent: () => import('./features/shop/shop.component'),
    providers: [
      //provideState({ name: 'shop', reducer: shopReducer}),
      provideState({ name: productsFeature.name, reducer: productsFeature.reducer }),
      provideState({ name: filtersFeature.name, reducer: filtersFeature.reducer }),
    ]
  },
  { path: 'cart', loadComponent: () => import('./features/cart.component')},
  { path: 'demo-counter', loadComponent: () => import('./features/demo-counter.component')},
  { path: '', redirectTo: 'home', pathMatch: 'full' }
];
