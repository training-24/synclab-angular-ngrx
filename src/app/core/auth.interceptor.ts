import { HttpInterceptorFn } from '@angular/common/http';
import { inject } from '@angular/core';
import { Store } from '@ngrx/store';
import { catchError, finalize, of, throwError } from 'rxjs';
import { ErrorsActions } from './store/errors/errors.actions';

export const authInterceptor: HttpInterceptorFn = (req, next) => {
  const store = inject(Store)
  store.dispatch(ErrorsActions.cleanError())
  return next(req)
    .pipe(
      catchError((err) => {
        switch(err.status) {
          case 404:
            break;
        }

        store.dispatch(ErrorsActions.error({ msg: 'error from intercetpor'}))
        return throwError(err)
      }),
    );
};
