import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { Product } from '../../../../model/product';

export const CartActions = createActionGroup({
  source: 'cart',
  events: {
    'add': props<{ item: Product }>(),
    'remove': props<{ id: string }>(),
  }
})
