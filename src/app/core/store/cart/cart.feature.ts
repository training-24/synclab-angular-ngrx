import { createFeature, createReducer, createSelector, on } from '@ngrx/store';
import { Product } from '../../../../model/product';
import { CartActions } from './cart.actions';

export interface CartState {
  list: Product[];
}

export const initialState: CartState = {
  list: []
}

export const cartFeature = createFeature({
  name: 'cart',
  reducer: createReducer(
    initialState,
    on(CartActions.add, (state, action) => {
      return {...state, list: [...state.list, action.item]  }
    }),
    on(CartActions.add, (state, action) => ({...state, list: [...state.list, action.item]  })),
    on(CartActions.remove, (state, action) => ({
      ...state,
      list: state.list.filter(item => item.id !== action.id)
    })),
  ),
  extraSelectors: ({ selectList }) => ({
    isEmpty: createSelector(
      selectList,
      (list) => list.length === 0
    ),
    totalItems: createSelector(
      selectList,
      (list) => list.length
    ),
    selectTotalCart: createSelector(
      selectList,
      (list) => list.reduce((total, item) => total + item.cost, 0)
    )
  })
})

export const {
  isEmpty,
  selectList,
  totalItems,
  selectTotalCart
} = cartFeature


