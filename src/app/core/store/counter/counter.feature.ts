import { createFeature, createReducer, createSelector, on } from '@ngrx/store';
import { CounterActions } from './counter.actions';

export interface CounterState {
  value: number,
  multiplier: number
};

const initialState: CounterState = {
  value: 1,
  multiplier: 10
}

export const counterFeature = createFeature({
  name: 'counter',
  reducer: createReducer(
    initialState,
    on(CounterActions.increment, (state ) => ({ ...state, value: state.value + 1} ) ),
    on(CounterActions.decrement, (state, action) => ({ ...state, value: state.value - action.value} ) ),
    on(CounterActions.reset, (state) => ({...state, value: 0})),
    on(CounterActions.changeMultiplier, (state, action) => ({...state, multiplier: action.value}))
  ),
  extraSelectors: ({ selectMultiplier, selectCounterState, selectValue }) => ({
      selectMultiplied: createSelector(
        selectValue,
        selectMultiplier,
        (value, multiplier) => value *  multiplier
      ),
      selectDouble: createSelector(
        selectValue,
        (value) => value * 2
      )
  })
})

export const {
  name,
  selectValue,
  selectMultiplied,
  selectDouble,
  selectCounterState
} = counterFeature

/*
export const selectMultipliedValue = createSelector(
  selectValue,
  selectMultiplier,
  (value, multiplier) => value *  multiplier
)*/

/*
export const selectMultipliedValue = (state: any) => {
  console.log(state)
  return state.counter.value * state.counter.multiplier
}
*/
