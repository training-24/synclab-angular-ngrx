import { createActionGroup, emptyProps, props } from '@ngrx/store';

export const ErrorsActions = createActionGroup({
  source: 'http',
  events: {
    'error': props<{ msg: string }>(),
    'clean error': emptyProps(),
  }
})
