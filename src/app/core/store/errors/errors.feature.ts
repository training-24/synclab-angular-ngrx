import { createFeature, createReducer, createSelector, on } from '@ngrx/store';
import { ErrorsActions } from './errors.actions';

export interface ErrorsState {
  msg: string,
};

const initialState: ErrorsState = {
  msg: '',
}

export const errorsFeature = createFeature({
  name: 'errors',
  reducer: createReducer(
    initialState,
    on(ErrorsActions.error, (state, action) => ({...state, msg: action.msg}))
  ),
})

export const {
  name,
  selectMsg
} = errorsFeature

/*
export const selectMultipliedValue = createSelector(
  selectValue,
  selectMultiplier,
  (value, multiplier) => value *  multiplier
)*/

/*
export const selectMultipliedValue = (state: any) => {
  console.log(state)
  return state.counter.value * state.counter.multiplier
}
*/
