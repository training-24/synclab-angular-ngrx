import { createAction, createActionGroup, emptyProps, props } from '@ngrx/store';

// export const go = createAction('[router] go', props<{ path: string}>())

export const RouterActions = createActionGroup({
  source: 'Router',
  events: {
    'go': props<{ path: string }>(),
    'back': emptyProps,
    'forward': emptyProps,
  }
})
