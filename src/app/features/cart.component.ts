import { Component, inject } from '@angular/core';
import { Store } from '@ngrx/store';
import { Product } from '../../model/product';
import { CartActions } from '../core/store/cart/cart.actions';
import { isEmpty, selectList as selectCartList, selectTotalCart, totalItems } from '../core/store/cart/cart.feature';

@Component({
  selector: 'app-cart',
  standalone: true,
  imports: [],
  template: `
    @for (item of cartItems(); track item.id) {
      <li>
        {{ item.name }} {{item.cost}}
        <button (click)="removeFromCartHandler(item)">remove</button>
      </li>
    } @empty {
      no items
    }
    <hr>
    
    @if(!isEmpty()) {
      TOTAL:€ {{totalCart()}}
    }
  `,

  styles: ``
})
export default class CartComponent {
  store = inject(Store)

  cartItems = this.store.selectSignal(selectCartList)
  isEmpty = this.store.selectSignal(isEmpty)
  totalItems = this.store.selectSignal(totalItems)
  totalCart = this.store.selectSignal(selectTotalCart)

  removeFromCartHandler(item: Product) {
    this.store.dispatch(CartActions.remove({ id: item.id}))
  }
}
