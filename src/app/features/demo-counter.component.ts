import { AsyncPipe, JsonPipe } from '@angular/common';
import { Component, computed, EventEmitter, inject } from '@angular/core';
import { Store } from '@ngrx/store';
import { CounterActions } from '../core/store/counter/counter.actions';
import { selectCounterState, selectDouble, selectMultiplied, selectValue } from '../core/store/counter/counter.feature';

@Component({
  selector: 'app-demo-counter',
  standalone: true,
  imports: [
    JsonPipe,
    AsyncPipe
  ],
  template: `
    <h1>Counter</h1>
    
    {{counter()}} - {{double()}} - {{multiplier()}}

    <button (click)="dec()">- </button>
    <button (click)="inc()">+ </button>
    <button (click)="reset()">reset</button>

    <button (click)="updateMultipler(5)">multiplier: 5 </button>
    <button (click)="updateMultipler(10)">multiplier: 10 </button>
  `,
  styles: ``
})
export default class DemoCounterComponent {
  store = inject(Store)
  counter = this.store.selectSignal(selectValue)
  counter$ = this.store.select(selectValue)
  double = this.store.selectSignal(selectDouble)
  multiplier = this.store.selectSignal(selectMultiplied)

  dec() {
    this.store.dispatch(CounterActions.decrement({  value: 2 }))
  }
  inc() {
    this.store.dispatch(CounterActions.increment())
  }
  reset() {
    this.store.dispatch(CounterActions.reset())
  }


  updateMultipler(value: number) {
    this.store.dispatch(CounterActions.changeMultiplier({ value }))

  }
}
