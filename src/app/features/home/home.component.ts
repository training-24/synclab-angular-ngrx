import { Component, inject } from '@angular/core';
import { TodosStore } from './store/todos.store';

@Component({
  selector: 'app-home',
  standalone: true,
  imports: [],
  template: `
    <p>
      home works!
    </p>
    <input
      type="text" #input
      (keydown.enter)="store.add(input.value); input.value = ''"
    >
    
    @if(!store.empty()) {
      <div>Done: {{store.doneTotal()}}</div>
      <div>Undone: {{store.undoneTotal()}}</div>
    }
    
    @for (todo  of store.list(); track todo.id) {
      <li>
        <input
          type="checkbox" [checked]="todo.done"
          (change)="store.toggle(todo.id)"
        >
        <span [style.text-decoration]="todo.done ? 'line-through' : 'none'">
            {{todo.title}}
        </span>
        <button (click)="store.remove(todo.id)">Delete</button>
      </li>
    } @empty {
      <div>No todos</div>
    }
    
    
    
  `,
  providers: [
    TodosStore
  ],
  styles: ``
})
export  default class HomeComponent {
  store = inject(TodosStore)
}
