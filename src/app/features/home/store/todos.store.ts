// features/demo-signals/store/todos.store.ts
import { computed, Injectable, Injector } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { patchState, signalStore, withComputed, withHooks, withMethods, withState } from '@ngrx/signals';
import { interval } from 'rxjs';

export const TodosStore = signalStore(
  withState({
    todos: [
      { id: 1, title: 'todo A', done: true },
      { id: 2, title: 'todo B', done: false },
      { id: 3, title: 'todo C', done: true },
    ]
  }),
  withComputed(({ todos }) => ({
    list: computed(() => todos()),
    empty: computed(() => todos().length === 0),
    doneTotal: computed(() => todos().filter(t => t.done).length),
    undoneTotal: computed(() => todos().filter(t => !t.done).length),
  })),
  withMethods((store) => ({
    add(title: string) {
      const newTodo: any = {
        title,
        id: Date.now(),
        done: false
      }
      patchState(store, { todos: [...store.todos(), newTodo]})
    },
    remove(id: number) {
      patchState(store, { todos: store.todos().filter(t => t.id !== id )});
    },
    toggle(id: number) {
      patchState(store, { todos: store.todos().map(t => t.id === id ? {...t, done: !t.done } : t )});
    }
  })),
  withHooks({
    onInit({ add }) {
      // interval(2_000)
      //   .pipe(takeUntilDestroyed())
      //   .subscribe(() => add('new todo'));
    },
    onDestroy({ todos }) {
      console.log('todos on destroy', todos());
    },
  }),
)

