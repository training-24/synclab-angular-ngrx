import { JsonPipe } from '@angular/common';
import { Component, computed, effect, inject, signal } from '@angular/core';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { Store } from '@ngrx/store';
import { debounceTime, startWith } from 'rxjs';
import { Product } from '../../../model/product';
import { CartActions } from '../../core/store/cart/cart.actions';
import { RouterActions } from '../../core/store/router/router.actions';
import { selectUrl } from '../../core/store/router/router.selectors';
import { FiltersActions } from './store/filters/filters.actions';
import { selectProductsFiltersState, selectText } from './store/filters/filters.feature';
import { ProductsActions } from './store/products/products.actions';
import { selectHasError, selectList, selectLoading } from './store/products/products.feature';
import { selectFilteredList } from './store/products/products.selectors';

@Component({
  selector: 'app-shop',
  standalone: true,
  imports: [
    JsonPipe,
    ReactiveFormsModule
  ],
  template: `
    <p>
      shop works! {{routerUrl()}}
    </p>

    @if (error()) {
      <div>ERRORE!</div>
    }
    
    @if(loading()) {  
      <div>loading....</div>
    }
    
    <input type="text" [formControl]="input"> 
    
    @for(product of products(); track product.id) {
      <li>
        {{product.name}}
        <button (click)="addToCartHandler(product)">add to cart</button>
      </li>
    }
    
    <hr>
    <button (click)="goHome()">BACK TO HOME</button>
    
  `,
  styles: ``
})
export default class ShopComponent {
  store = inject(Store)
  products = this.store.selectSignal(selectFilteredList)
  loading = this.store.selectSignal(selectLoading)
  error = this.store.selectSignal(selectHasError)
  filterText = this.store.selectSignal(selectText)
  routerUrl = this.store.selectSignal(selectUrl)

  input = new FormControl('', { nonNullable: true })

  constructor() {
    effect(() => {
      if (this.filterText() !== this.input.value) {
        this.input.setValue(this.filterText(), { emitEvent: false });
      }
    });
  }

  ngOnInit() {
    this.store.dispatch(ProductsActions.load());

    this.input.valueChanges
      .pipe(
        debounceTime(700)
      )
      .subscribe(text =>{
        this.store.dispatch(FiltersActions.search({ text }))
        // ALTERNARTIVA:
        // this.store.dispatch(FiltersActions.setFilters({ filters: { text: text, gender: 'F'} }))
      })

  }

  addToCartHandler(item: Product) {
    this.store.dispatch(CartActions.add({ item }))
  }

  ngOnDestroy() {
    this.store.dispatch(ProductsActions.reset());
  }


  goHome() {
    this.store.dispatch(RouterActions.go({ path: '/home'}))
  }
}
