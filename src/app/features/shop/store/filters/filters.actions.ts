import { createActionGroup, props } from '@ngrx/store';
import { FiltersState } from './filters.feature';


export const FiltersActions = createActionGroup({
  source: 'shop filters',
  events: {
    'Search': props<{ text: string }>(),
    'setFilters': props<{ filters: Partial<FiltersState> }>()
  }
})
