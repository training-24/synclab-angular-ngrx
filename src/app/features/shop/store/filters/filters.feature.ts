import { createFeature, createReducer, on } from '@ngrx/store';
import { FiltersActions } from './filters.actions';

export interface FiltersState {
  text: string;
  gender: 'M' | 'F';
  age: number,
  // ...
}

export const initialState: FiltersState = {
  text: '',
  gender: 'M',
  age: 18
}
export const filtersFeature = createFeature({
  name: 'productsFilters',
  reducer: createReducer(
    initialState,
    on(FiltersActions.search, (state, action) => ({ ...state, text: action.text })),
    on(FiltersActions.setFilters, (state, action) => ({ ...state, ...action.filters}))
  )
})

export const {
  selectProductsFiltersState,
  selectText
} = filtersFeature


