import { combineReducers } from '@ngrx/store';
import { filtersFeature } from './filters/filters.feature';
import { productsFeature } from './products/products.feature';

export const shopReducer = combineReducers({
  'products': productsFeature.reducer,
  'filters': filtersFeature.reducer,

})
