import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { Product } from '../../../../../model/product';

export const ProductsActions = createActionGroup({
  source: 'products',
  events: {
    'reset': emptyProps(),
    'load': emptyProps(),
    'load success': props<{ items: Product[] }>(),
    'load failed': emptyProps(),

    // 'add': props<{ item: Omit<Product, 'id'> }>,
    'add': props<{ item: Product }>,
    'add success': props<{ item: Product }>(),
    'Add Product Fail': emptyProps(),

    'Delete Product': props<{ id: number }>(),
    'Delete Product Success': props<{ id: number }>(),
    'Delete Product Fail': emptyProps(),
  }

})

