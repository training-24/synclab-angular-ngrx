import { HttpClient } from '@angular/common/http';
import { inject } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { createAction, props } from '@ngrx/store';
import { catchError, delay, map, mergeMap, of, tap } from 'rxjs';
import { Product } from '../../../../../model/product';
import { ErrorsActions } from '../../../../core/store/errors/errors.actions';

import { ProductsActions } from './products.actions';

export const loadProducts = createEffect(
(
  http = inject(HttpClient),
  actions$ = inject(Actions),
  router = inject(Router)
) => {
      // http.get()
    // http..localstorage
    return actions$
      .pipe(
        ofType(ProductsActions.load),
        // map(() => {ProductsActions.loadSuccess({ items: [] }))
        mergeMap(
          () => http.get<Product[]>('http://localhost:3000/products')
            .pipe(
              delay(300),
              map((items) => {
                return ProductsActions.loadSuccess({ items })
              }),
              catchError(() => of(ProductsActions.loadFailed()))
              // catchError(() => of(ErrorsActions.error({ msg: 'Dati non caricati'})))
            )
        )
      )
  },
  {
    functional: true
  }
)

/*

export const loadProductsSuccess = createEffect(
(
  actions$ = inject(Actions),
) => {
    return actions$
      .pipe(
        ofType(ProductsActions.loadSuccess),
        map(() => go({ path: 'home'}))
      )
  },
  {
    functional: true
  }
)


*/

