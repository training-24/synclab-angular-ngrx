import { createFeature, createReducer, on } from '@ngrx/store';
import { Product } from '../../../../../model/product';
import { ProductsActions } from './products.actions';

export interface ProductsState {
  list: Product[];
  hasError: boolean;
  loading: boolean
}

export const initialState: ProductsState = {
  list: [],
  loading: false,
  hasError: false,
}


export const productsFeature = createFeature({
  name: 'products',
  reducer: createReducer(
    initialState,
    on(ProductsActions.reset, (state, action) => ({ ...state, list: [], loading: false})),
    on(ProductsActions.load, (state, action) => ({ ...state, loading: true, hasError: false})),
    on(ProductsActions.loadSuccess, (state, action) => ({
      ...state,
      list: action.items,
      loading: false
    })),
    on(ProductsActions.loadFailed, (state, action) => ({
      ...state,
      list: [],
      loading: false,
      hasError: true
    }))
  )
})

export const {
  selectList,
  selectLoading,
  selectHasError,
  selectProductsState
} = productsFeature
