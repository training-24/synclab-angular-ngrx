import { createSelector } from '@ngrx/store';
import { selectText } from '../filters/filters.feature';
import { selectList } from './products.feature';

export const selectFilteredList = createSelector(
  selectList,
  selectText,
  (list, text) => list.filter(item => {
    return item.name.toLowerCase().includes(text.toLowerCase())
  })
)
